DEMO DOCKER & DOCKER-COMPOSE
---

Cette aplication présente une simple interface utilisateur utilisant
- index.html avec du javascript et css
- nodejs en backend avec module express
- mongodb pour le stockage

### Comment utiliser l'application

#### Avec Docker

**Step 1: Créer un réseau docker**

    docker network create demo-network 

**Step 2: Démarrer mongodb** 

    docker container run -d -p 27017:27017 \
    -e MONGO_INITDB_ROOT_USERNAME=admin \
    -e MONGO_INITDB_ROOT_PASSWORD=password \
    --name mongodb \
    --net demo-network \
    mongo    

**Step 3: Démarrer mongo-express**
    
    docker container run -d -p 8081:8081 \
    -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
    -e ME_CONFIG_MONGODB_ADMINPASSWORD=password \
    -e ME_CONFIG_MONGODB_SERVER=mongodb \
    --net demo-network \
    --name mongo-express \ 
    mongo-express   

_**NOTE:** La création du réseau est optionnel. Vous pouvez juste démarrer les conteneurs dans le réseau par défaut.
Dans ce cas, ne créez pas de réseau et retirez `--net` de la commande `docker container run`_ <br>

**Step 4: Ouvrez mongo-express depuis le navigateur**

    http://localhost:8081 OU http://server_ip:8081

**Step 5: Créer l'image de l'application**

    cd docker/demo
    docker build -t node-app .

**Step 6: Créer le conteneur de l'application**

    docker container run -d -p 3000:3000 \
    --name app \
    --net demo-network \
    -e MONGO_USERNAME=admin \
    -e MONGO_PWD=password \
    -e MONGO_HOST=${mongo_container} \
    node-app
    
**Step 7: Ouvrez votre application nodejs dans le navigateur**

    http://localhost:3000 OU http://server_ip:3000


#### Avec Docker Compose

**Step 1: Démarrer tous les services**

    docker-compose -f docker-compose.yaml up -d

**Step 2: Accéder à votre application depuis le navigateur** 

    http://<adresse_ip>:3000

**Step 3: Accéder à mongo-express depuis le navigateur** 

    http://<adresse_ip>:8081


